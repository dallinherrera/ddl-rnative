import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import Question from './question';

class Game extends React.Component {
  constructor(props){
    super(props);
    this.secondsAllowed = 5;
    this.state = {
      currentSecond: this.secondsAllowed,
      currentQuestionNumber: 0,
      questions: [
        {
          image_name: 'primera-vision.png',
          question: 'Que personaje está de rodillas?',
          answA: 'Jose Smith',
          answB: 'Brigham Young',
          answC: 'Abraham',
          answD: 'Moises'
        },
        {
          image_name: 'moroni.jpg',
          question: 'Que personaje es la estatua arriba de los templos?',
          answA: 'Moroni',
          answB: 'EL angel del apocalipsis',
          answC: 'Jesús',
          answD: 'Jose Smith'
        },
        {
          image_name: 'diego.jpg',
          question: 'A este personaje se le apareció la virgen de Guadalupe.',
          answA: 'Juan Diego',
          answB: 'Lázaro',
          answC: 'Cristobal Colón',
          answD: 'Hernan Cortés'
        }
      ],
    };
    // Change quest_num to current_question_number
    this.nextQuestion = this.nextQuestion.bind(this);
    // Change to nextQuestion
    // current_question currentQuestion
    // Change counter to timer
    this.showScore = this.showScore.bind(this);
  }

  showScore() {
    this.props.showScore();
  }

  componentDidMount() {
    this.timerID = window.setInterval(() => {
      if(this.state.currentSecond===0){
        this.nextQuestion();
      }
      else {
        this.setState((state, props) => {
          const seconds = state.currentSecond - 1;
          return { currentSecond: seconds }
        });
      }
    }, 1000)
  }

  nextQuestion() {
    if(this.state.currentQuestionNumber < this.state.questions.length -1) {
      this.setState((state, props) => {
        const questionNumber = state.currentQuestionNumber + 1;
        return {
          currentSecond: this.secondsAllowed,
          currentQuestionNumber: questionNumber
        }
      });
    }
    else {
      this.props.showScore();
      clearInterval(this.timerID);
    }

  }


  render() {
    const currentQuestion = this.state.questions[this.state.currentQuestionNumber];
    return (
      <View id="game">
        <Question
          {...currentQuestion}
          currentSecond={this.state.currentSecond}
          nextQuestionFn={this.nextQuestion}
          image_name={this.state.questions.image_name}
        />
      </View>
    );
  }
}

export default Game;
