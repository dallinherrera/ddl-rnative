import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class Welcome extends React.Component {
  render() {
    return (
      <View style={styles.contenedor}>
        <Text h1 style={styles.titulo}>Doctores de la Ley</Text>
        <Text style={styles.text}>
          Qué tanto sabes de las santas escrituras?
        </Text>
        <Text style={styles.text}>
          Pon a prueba tus conocomientos y empieza a jugar ya!
        </Text>
        <Button title="Comenzar a jugar" onPress={this.props.startPlaying} />
      </View>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#fff',
    marginTop: '50%',
    marginLeft: '5%',
    marginRight: '5%',
    marginBottom: '5%',
  },

  titulo: {
    fontSize: 25,
    textAlign: 'center',
    fontWeight: 'bold',
    marginBottom: 5,
  },

  text: {
    color: '#1c1b1b',
    fontSize: 22,
    textAlign: 'center',
    marginBottom: '5%',
  },
});
