import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';

class Score extends React.Component {
  render() {
    return (
      <View id="score" className="text-center">
        <Text h1>Thanks for your participation!</Text>
        <Image source={require('./images/congrats.gif')} style={{width:100, height: 100}} alt="congrats" />
      </View>
    );
  }
}

export default Score;
