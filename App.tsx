import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import Welcome from './welcome';
import Game from './game';
import Score from './score';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state={ route:'welcome' };
    this.startPlaying = this.startPlaying.bind(this);
    this.showScore = this.showScore.bind(this);
  }

  startPlaying (e) {
    e.preventDefault();
    this.setState({ route:'game' });
  }

  showScore() {
    this.setState({ route:'score' });
  }

  render() {

    if(this.state.route === 'game') {
      return(
        <View>
          <Game showScore={this.showScore}/>
        </View>
      )
    }
    else if(this.state.route === 'score') {
      return (
        <View>
          <Score />
        </View>
      )
    }

    return (
      <View style={styles.appContainer}>
        <Welcome startPlaying={this.startPlaying}/>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  appContainer: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
