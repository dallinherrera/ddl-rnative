import React from 'react';
import { StyleSheet, Text, View, Button, Image } from 'react-native';
import Timer from './timer';
import Game from './game';

class Question extends React.Component {
  constructor(props) {
    super(props);
    this.submitAnswer = this.submitAnswer.bind(this);
  }


  submitAnswer(e) {
    this.props.nextQuestionFn();
    console.log(e.target);
  }


  render() {
    let answers = [this.props.answA, this.props.answB, this.props.answC, this.props.answD];

    return (
      <View id="questions" style={{alignItems:'center'}}>
        <Timer seconds={this.props.currentSecond}/>
        <Image source={this.props.image_name} style={styles.img_style} />

        <View>
          <Text style={styles.p_question}>{this.props.question}</Text>
          {answers.map((answer, index) => <Text key={index} style={styles.p_answer} onPress={this.submitAnswer}>{answer}</Text>)}
        </View>
      </View>
    );
  }
}

export default Question;

const styles = StyleSheet.create( {
  img_style: {
    width: 300,
    maxWidth: 300,
    height: 300,
    minHeight: 300,
    maxHeight: 300,
    borderColor: '#1c293c',
    borderWidth: 0.5,
    borderRadius: 3,
    margin: 5,
    flex: 1,
  },
  p_question: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    flex: 0.5,
  },
  p_answer: {
    borderWidth: 0.5,
    borderColor: '#b2aa7a',
    borderRadius: 12,
    backgroundColor: '#f0e6ca',
    lineHeight: 30,
    textAlign: 'center',
  },
});
