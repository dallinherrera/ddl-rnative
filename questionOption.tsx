import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

export default function QuestionOption(props) {
  return (
      <Text key={props.key}>{props.answer}</Text>
  )
}
