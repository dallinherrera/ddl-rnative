import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class Timer extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
        <Text id="timer" style={styles.timerContainer}>
          Te quedan    <Text style={styles.timerCounter}>{this.props.seconds} </Text> segundos.
        </Text>
    );
  }
}

export default  Timer;

const styles = StyleSheet.create({
  timerContainer: {
    backgroundColor: '#fff',
    lineHeight: 80,
    textAlign: 'center',
  },
  timerCounter: {
    color: 'red',
    fontWeight:'900',
    fontSize: 40,
    margin: 10,
  },

});
